Tutorial on Basic APIs
----------------------

.. include:: replace.txt
.. highlight:: cpp

This section contains a list of example programs demonstrating the 
basic APIs used in |ns3|.

If you downloaded the system as was suggested above, you will have a release
of |ns3| in a directory called ``workspace`` under your home
directory.  Change into that release directory, and you should find a
directory structure something like the following:

.. sourcecode:: bash

  AUTHORS        CMakeLists.txt   examples   RELEASE_NOTES.md  testpy.supp
  bindings       contrib          LICENSE    scratch           utils
  build-support  CONTRIBUTING.md  ns3        src               utils.py
  CHANGES.md     doc              README.md  test.py           VERSION

Change into the ``examples/tutorial`` directory. 
The directory contains the following examples to get started,
and we recommend that beginners walk through these examples in the same
order as they are presented:

.. toctree::
    :maxdepth: 2

    index
    