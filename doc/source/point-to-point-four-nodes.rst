This is an example to demonstrate point-to-point networks that connect hosts 
``L0, L1, L2...`` with ``R0, R1, R2...`` via two routers ``r0`` and ``r1``.
The nodes are given IPv4 addresses. The link has asymmetric bandwidth and delay.
A ping application is used to test connectivity of the nodes.

Overview
========
.. literalinclude:: point-to-point-four-nodes.cc
   :language: cpp
   :start-after: start-general-documentation
   :end-before: end-general-documentation

Code
====

.. literalinclude:: point-to-point-four-nodes.cc
   :linenos:
   :language: cpp
   :start-after: code-body