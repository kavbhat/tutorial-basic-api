/*
 * Copyright (c) 2023 NITK Surathkal
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Amogh Umesh <amoghumesh02@gmail.com>
 */

// start-general-documentation
/*
 * Two LANs with 4 nodes each connected via two routers, asymmetric link
 * bandwidth and delay, IPv4 addresses, Ping application.
 *
 * ////////////////////////////////////////////////////////////////////////////////////////////////////
 * //                                        Network Topology                                        //
 * //                                                                                                //
 * //     10.1.0.0         10.1.1.0              10.1.2.0              10.1.3.0         10.1.4.0     //
 * //   100Mbps, 1ms     5Mbps, 2ms -->        5Mbps, 2ms -->        5Mbps, 2ms -->   100Mbps, 1ms   //
 * //  n5  n4  n3  n2 ------------------ n0 ------------------ n1 ------------------ n6  n7  n8  n9  //
 * //   |  |   |   |    <-- 10Mbps, 2ms       <-- 10Mbps, 2ms      <-- 10Mbps, 2ms   |   |   |   |   //
 * // ================                                                              ================ //
 * //      LAN 1             link 1                link 2                link 3           LAN 2      //
 * //                    point-to-point        point-to-point        point-to-point                  //
 * ////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Five Ping packets are sent from n3 to n7, and the success/failure of these
 * packets is reported.
 * The output will look like this:
 *
 * PING 10.1.4.4 - 56 bytes of data; 84 bytes including ICMP and IPv4 headers.
 * 64 bytes from 10.1.4.4: icmp_seq=0 ttl=60 time=43.7 ms
 * 64 bytes from 10.1.4.4: icmp_seq=1 ttl=60 time=16.652 ms
 * 64 bytes from 10.1.4.4: icmp_seq=2 ttl=60 time=16.652 ms
 * 64 bytes from 10.1.4.4: icmp_seq=3 ttl=60 time=16.652 ms
 * 64 bytes from 10.1.4.4: icmp_seq=4 ttl=60 time=16.652 ms
 *
 * --- 10.1.4.4 ping statistics ---
 * 5 packets transmitted, 5 received, 0% packet loss, time 4016ms
 * rtt min/avg/max/mdev = 16/21.4/43/12.07 ms
 *
 * The example program does not produce any output files, and does not provide
 * options for configuration via the command line.
 */
// end-general-documentation

#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-apps-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"

// code-body
using namespace ns3;

int
main(int argc, char* argv[])
{
    /*
     * Allows to parse command line arguments
     */
    CommandLine cmd;

    /*
     * senderNodeIndex and receiverNodeIndex will have a default value of 3(n3) & 7(n7) which will
     * be changed if argument is used.
     */
    int senderNodeIndex = 3;
    int receiverNodeIndex = 7;

    /*
     * The 'AddValue' API adds a program argument. It takes the name of
     * program-supplied argument, the help text displayed when program is run
     * with "--help" argument and a value that will reference the variable
     * whose value is to be set.
     *
     * Command line arguments can be invoked in the following way:
     * ./ns3 run "two-lans-tracing.cc --sender=1 --receiver=7"
     */

    cmd.AddValue("sender", "Index of the sender node", senderNodeIndex);
    cmd.AddValue("receiver", "Index of the receiver node", receiverNodeIndex);

    cmd.Usage("Tutorial example to demonstrate two LANs connected by a network");
    cmd.Parse(argc, argv);

    // Create the 10 nodes n0, n1, n2... n9
    NodeContainer nodes;
    nodes.Create(10);

    // Add the router nodes n0, n1 to the container routerNodes
    NodeContainer routerNodes;
    routerNodes.Add(nodes.Get(0));
    routerNodes.Add(nodes.Get(1));

    // Add the CSMA LAN 1 nodes n2, n3, n4, n5 to the container csmaLan1Nodes
    NodeContainer csmaLan1Nodes;
    csmaLan1Nodes.Add(nodes.Get(2));
    csmaLan1Nodes.Add(nodes.Get(3));
    csmaLan1Nodes.Add(nodes.Get(4));
    csmaLan1Nodes.Add(nodes.Get(5));

    // Add the CSMA LAN 2 nodes n6, n7, n8, n9 to the container csmaLan2Nodes
    NodeContainer csmaLan2Nodes;
    csmaLan2Nodes.Add(nodes.Get(6));
    csmaLan2Nodes.Add(nodes.Get(7));
    csmaLan2Nodes.Add(nodes.Get(8));
    csmaLan2Nodes.Add(nodes.Get(9));

    /*
     * Note: While CSMA (Carrier Sense Multiple Access) is a valuable protocol for simulating
     * traditional LANs with shared bandwidth, many modern Ethernet networks do not share the
     * cable and instead use switches to isolate nodes.
     */

    /*
     * Create a CsmaHelper. The attribute DataRate is set to 100Mbps and
     * Delay is set to 1ms.
     */
    CsmaHelper csma;
    csma.SetChannelAttribute("DataRate", StringValue("100Mbps"));
    csma.SetChannelAttribute("Delay", StringValue("1ms"));

    // Create NetDeviceContainers for the csma devices on lan1 and lan2.
    NetDeviceContainer csmaLan1Devices;
    csmaLan1Devices = csma.Install(csmaLan1Nodes);

    NetDeviceContainer csmaLan2Devices;
    csmaLan2Devices = csma.Install(csmaLan2Nodes);

    /*
     * Creates a PointToPointHelper, with the attribute Delay set to 2ms.
     */
    PointToPointHelper pointToPoint;
    pointToPoint.SetChannelAttribute("Delay", StringValue("2ms"));

    // Create NetDeviceContainers for the devices on link1, link2 and link3.
    NetDeviceContainer link1Devices;
    link1Devices = pointToPoint.Install(csmaLan1Nodes.Get(0), routerNodes.Get(0));
    link1Devices.Get(0)->GetObject<PointToPointNetDevice>()->SetAttribute("DataRate",
                                                                          StringValue("5Mbps"));
    link1Devices.Get(1)->GetObject<PointToPointNetDevice>()->SetAttribute("DataRate",
                                                                          StringValue("10Mbps"));

    NetDeviceContainer link2Devices;
    link2Devices = pointToPoint.Install(routerNodes);
    link2Devices.Get(0)->GetObject<PointToPointNetDevice>()->SetAttribute("DataRate",
                                                                          StringValue("5Mbps"));
    link2Devices.Get(1)->GetObject<PointToPointNetDevice>()->SetAttribute("DataRate",
                                                                          StringValue("10Mbps"));

    NetDeviceContainer link3Devices;
    link3Devices = pointToPoint.Install(routerNodes.Get(1), csmaLan2Nodes.Get(0));
    link3Devices.Get(0)->GetObject<PointToPointNetDevice>()->SetAttribute("DataRate",
                                                                          StringValue("5Mbps"));
    link3Devices.Get(1)->GetObject<PointToPointNetDevice>()->SetAttribute("DataRate",
                                                                          StringValue("10Mbps"));

    // Install the internet stack on all the nodes
    InternetStackHelper stack;
    stack.Install(nodes);

    /*
     * Assign IPv4 addresses to all the interfaces.
     * This example has 5 networks:
     *      - LAN 1 consisting of n2, n3, n4 and n5 with network id 10.1.0.0/24
     *      - link 1 consisting of n2 and n0 with network id 10.1.1.0/24
     *      - link 2 consisting of n0 and n1 with network id 10.1.2.0/24
     *      - link 3 consisting of n1 and n6 with network id 10.1.3.0/24
     *      - LAN 2 consisting of n6, n7, n8 and n9 with network id 10.1.4.0/24
     */

    // csma LAN 1
    Ipv4AddressHelper csmaLan1Address;
    csmaLan1Address.SetBase("10.1.0.0", "255.255.255.0");
    Ipv4InterfaceContainer csmaLan1Interfaces = csmaLan1Address.Assign(csmaLan1Devices);

    // link 1
    Ipv4AddressHelper link1Address;
    link1Address.SetBase("10.1.1.0", "255.255.255.0");
    Ipv4InterfaceContainer link1Interfaces = link1Address.Assign(link1Devices);

    // link 2
    Ipv4AddressHelper link2Address;
    link2Address.SetBase("10.1.2.0", "255.255.255.0");
    Ipv4InterfaceContainer link2Interfaces = link2Address.Assign(link2Devices);

    // link 3
    Ipv4AddressHelper link3Address;
    link3Address.SetBase("10.1.3.0", "255.255.255.0");
    Ipv4InterfaceContainer link3Interfaces = link3Address.Assign(link3Devices);

    // csma LAN 2
    Ipv4AddressHelper csmaLan2Address;
    csmaLan2Address.SetBase("10.1.4.0", "255.255.255.0");
    Ipv4InterfaceContainer csmaLan2Interfaces = csmaLan2Address.Assign(csmaLan2Devices);

    /*
     * Build a routing database and initialize the routing tables of the nodes
     * in the simulation.
     */
    Ipv4GlobalRoutingHelper::PopulateRoutingTables();

    /*
     * Ping from node with index senderNodeIndex to node with index receiverNodeIndex
     * Get the first Address of the first Interface (not including loopback) of both the nodes
     * GetAddress takes in the index of the interface and index of ip address as argument
     * Since Loopback is index 0 we use 1 for interface and 0 for address
     */
    Address destination = nodes.Get(receiverNodeIndex)
                              ->GetObject<ns3::Ipv4>()
                              ->GetAddress(1, 0)
                              .GetLocal(); // destination
    Address source =
        nodes.Get(senderNodeIndex)->GetObject<ns3::Ipv4>()->GetAddress(1, 0).GetLocal(); // source

    // Create a ping application with receiverNodeIndex as destination and senderNodeIndex as
    // source.
    PingHelper pingHelper(destination, source);
    pingHelper.SetAttribute("Interval", TimeValue(Seconds(1.0)));
    pingHelper.SetAttribute("Size", UintegerValue(56));
    pingHelper.SetAttribute("Count", UintegerValue(5));

    // Install the ping application on node n3.
    ApplicationContainer apps = pingHelper.Install(nodes.Get(senderNodeIndex));
    apps.Start(Seconds(1));
    apps.Stop(Seconds(6));

    /*
     * Set the stop time for the simulator. Run the simulation, and invoke
     * 'Destroy' at the end of the simulation.
     */
    Simulator::Stop(Time(Seconds(6.0)));
    Simulator::Run();
    Simulator::Destroy();
    return 0;
}
