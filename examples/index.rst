Basic examples to get started
-----------------------------
.. include:: replace.txt

General Introduction 
====================

ns3 Namespace
+++++++++++++
::

  using namespace ns3;

The |ns3| project is implemented in a C++ namespace called
``ns3``.  This groups all |ns3|-related declarations in a scope
outside the global namespace, which we hope will help with integration with
other code.  The C++ ``using`` statement introduces the |ns3|
namespace into the current (global) declarative region.  This is a fancy way
of saying that after this declaration, you will not have to type ``ns3::``
scope resolution operator before all of the |ns3| code in order to use
it.  If you are unfamiliar with namespaces, please consult almost any C++
tutorial and compare the ``ns3`` namespace and usage here with instances of
the ``std`` namespace and the ``using namespace std;`` statements you
will often find in discussions of ``cout`` and streams.

Module Includes
+++++++++++++++
The code proper starts with a number of include statements.

::

  #include "ns3/core-module.h"
  #include "ns3/network-module.h"
  #include "ns3/internet-module.h"
  #include "ns3/point-to-point-module.h"
  #include "ns3/applications-module.h"

To help our high-level script users deal with the large number of include
files present in the system, we group includes according to relatively large
modules.  We provide a single include file that will recursively load all of
the include files used in each module.  Rather than having to look up exactly
what header you need, and possibly have to get a number of dependencies right,
we give you the ability to load a group of files at a large granularity.  This
is not the most efficient approach but it certainly makes writing scripts much
easier.

Each of the |ns3| include files is placed in a directory called
``ns3`` (under the build directory) during the build process to help avoid
include file name collisions.  The ``ns3/core-module.h`` file corresponds
to the ns-3 module you will find in the directory ``src/core`` in your
downloaded release distribution.  If you list this directory you will find a
large number of header files.  When you do a build, ns3 will place public
header files in an ``ns3`` directory under the appropriate
``build/debug`` or ``build/optimized`` directory depending on your
configuration.  CMake will also automatically generate a module include file to
load all of the public header files.

Since you are, of course, following this tutorial religiously, you will
already have run the following command from the top-level directory:

.. sourcecode:: bash

  $ ./ns3 configure -d debug --enable-examples --enable-tests

in order to configure the project to perform debug builds that include
examples and tests.  You will also have called

.. sourcecode:: bash

  $ ./ns3 build

to build the project.  So now if you look in the directory
``../../build/include/ns3`` you will find the four module include files shown
above (among many other header files).  You can take a look at the contents of 
these files and find that they do include all of the public include files in 
their respective modules.

Using Command Line Arguments
++++++++++++++++++++++++++++

Another way you can change how ns-3 scripts behave without editing and building 
is via command line arguments. We provide a mechanism to parse command line 
arguments and automatically set local and global variables based on those 
arguments.

The first step in using the command line argument system is to declare the 
command line parser. This is done quite simply in your main program as in the 
following code:

::

  int
  main(int argc, char *argv[])
  {
    ...

    CommandLine cmd;
    cmd.Parse(argc, argv);

    ...
  }

This simple two line snippet is actually very useful by itself.  It opens the
door to the |ns3| global variable and ``Attribute`` systems.

To run an example, such as ``point-to-point-two-nodes.cc``, run the following 
command from the top-level directory:

.. sourcecode:: bash

  $ ./ns3 run point-to-point-two-nodes

To view the usage guide for this example, run the following command:

.. sourcecode:: bash

  $ ./ns3 run 'point-to-point-two-nodes --PrintHelp'

This will ask ns3 to run the ``point-to-point-two-nodes`` script and pass the 
command line argument ``--PrintHelp`` to the script.  The quotes are required to
sort out which program gets which argument. This provides a short explanation 
of the program and command line arguments that may be passed to the program.

1. point-to-point-two-nodes.cc
==============================
.. include:: point-to-point-two-nodes.rst
    
2. point-to-point-three-nodes.cc
================================
.. include:: point-to-point-three-nodes.rst

3. point-to-point-four-nodes.cc
===============================
.. include:: point-to-point-four-nodes.rst

4. two-lans-point-to-point.cc
=================================
.. include:: two-lans-point-to-point.rst

5. two-lans-tracing.cc
======================
.. include:: two-lans-tracing.rst